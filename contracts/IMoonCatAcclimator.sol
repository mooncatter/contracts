// SPDX-License-Identifier: GPL-3.0-only
pragma solidity ^0.8.9;

interface IERC165 {
  function supportsInterface (bytes4 interfaceId) external view returns (bool);
}

interface IERC721Events {
  event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
  event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
  event ApprovalForAll(address indexed owner, address indexed operator, bool approved);
}

interface IERC721 is IERC165 {
  function balanceOf (address owner) external view returns (uint256 balance);
  function ownerOf (uint256 tokenId) external view returns (address owner);
  function safeTransferFrom (
    address from,
    address to,
    uint256 tokenId
  ) external;
  function transferFrom (
    address from,
    address to,
    uint256 tokenId
  ) external;
  function approve (address to, uint256 tokenId) external;
  function getApproved (uint256 tokenId) external view returns (address operator);
  function setApprovalForAll (address operator, bool _approved) external;
  function isApprovedForAll (address owner, address operator) external view returns (bool);
  function safeTransferFrom (
    address from,
    address to,
    uint256 tokenId,
    bytes calldata data
  ) external;
}

interface IERC721Metadata is IERC721 {
  function name () external view returns (string memory);
  function symbol () external view returns (string memory);
  function tokenURI (uint256 tokenId) external view returns (string memory);
}

interface IERC721Enumerable is IERC721 {
  function totalSupply () external view returns (uint256);
  function tokenOfOwnerByIndex (address owner, uint256 index) external view returns (uint256 tokenId);
  function tokenByIndex (uint256 index) external view returns (uint256);
}

/**
 * @title MoonCat​Acclimator
 * @notice Accepts an original MoonCat and wraps it to present an ERC721- and ERC998-compliant asset
 * @notice Accepts a MoonCat wrapped with the older wrapping contract (at 0x7C40c3...) and re-wraps them
 * @notice Ownable by an admin address. Rights of the Owner are to pause and unpause the contract, and update metadata URL
 */
interface IMoonCatAcclimator is IERC721Enumerable, IERC721Metadata {

  function pause () external;

  function unpause () external;

  event MoonCatAcclimated(uint256 tokenId, address indexed owner);

  event MoonCatDeacclimated(uint256 tokenId, address indexed owner);

  function wrap (uint256 _rescueOrder) external;

  function buyAndWrap (uint256 _rescueOrder) external payable;

  function unwrap (uint256 _tokenId) external;

  function onERC721Received (
    address _operator,
    address _from,
    uint256 _oldTokenId,
    bytes calldata _data
  ) external returns (bytes4);

  function setBaseURI (string memory _newBaseURI) external;

  function batchReWrap (
    uint256[] memory _rescueOrders,
    uint256[] memory _oldTokenIds
  ) external;

  function batchWrap (uint256[] memory _rescueOrders) external;

  function batchUnwrap (uint256[] memory _rescueOrders) external;

  function tokensIdsByOwner (address owner) external view returns (uint256[] memory);

  function getChild (
    address _from,
    uint256 _tokenId,
    address _childContract,
    uint256 _childTokenId
  ) external;

  function ownerOfChild (address _childContract, uint256 _childTokenId)
    external
    view
    returns (bytes32 parentTokenOwner, uint256 parentTokenId);

  function rootOwnerOf (uint256 _tokenId) external view returns (bytes32 rootOwner);

  function rootOwnerOfChild (address _childContract, uint256 _childTokenId)
    external
    view
    returns (bytes32 rootOwner);

  function safeTransferChild (
    uint256 _fromTokenId,
    address _to,
    address _childContract,
    uint256 _childTokenId
  ) external;

  function safeTransferChild (
    uint256 _fromTokenId,
    address _to,
    address _childContract,
    uint256 _childTokenId,
    bytes calldata _data
  ) external;

  function transferChild (
    uint256 _fromTokenId,
    address _to,
    address _childContract,
    uint256 _childTokenId
  ) external;

  function transferChildToParent (
    uint256 _fromTokenId,
    address _toContract,
    uint256 _toTokenId,
    address _childContract,
    uint256 _childTokenId,
    bytes calldata _data
  ) external;

  function totalChildContracts (uint256 _tokenId) external view returns (uint256);

  function childContractByIndex (uint256 _tokenId, uint256 _index)
    external
    view
    returns (address childContract);

  function totalChildTokens (uint256 _tokenId, address _childContract)
    external
    view
    returns (uint256);

  function childTokenByIndex (
    uint256 _tokenId,
    address _childContract,
    uint256 _index
  ) external view returns (uint256 childTokenId);

  // INHERITED FROM ERC998
  event ReceivedChild(
    address indexed _from,
    uint256 indexed _toTokenId,
    address indexed _childContract,
    uint256 _childTokenId
  );
  event TransferChild(
    uint256 indexed _fromTokenId,
    address indexed _to,
    address indexed _childContract,
    uint256 _childTokenId
  );
}
