A collection of smart contract templates, for working with existing infrastructure contracts in the MoonCatRescue ecosystem.

# Usage
To add this module to an existing Node.js application:

    npm install --save-dev @mooncatrescue/contracts

Then within a Solidity contract, you can import an interface by referencing it like this:

```solidity
// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

import { IMoonCatRescue } from "@mooncatrescue/contracts/IMoonCatRescue.sol";
```

Within a Hardhat script or test file, the `moonCatUtils` helper library can be imported like:


```js
const { network, ethers } = require('hardhat'),
  { disperseMoonCats } = require('@mooncatrescue/contracts/moonCatUtils');

async function main() {
  await disperseMoonCats(1000, 1004, 5); // Give one MoonCat to the first five addresses
  await disperseMoonCats(5050, 5059, 2); // Give five more MoonCats to the first two addresses
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
```